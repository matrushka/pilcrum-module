# Pilcrum

This module provides content and paragraphs types for the Pilcrum museum publishing platform.

Content types:

* Object: a generic content type for collection objects that allows linking them with Images, Videos and Audio representations.
* Audio
* Image
* Image gallery
* Video
* Story

Metadata:
To hold the metadata for the Object, Audio, Image and Video nodes, the Metadata module is in use. This module enables the Dublin Core, Open Graph and Twitter Card metadata sets, but the development of other metadata standards is possible.

Flexibility:
The files provided in the content types of this module can be used to build extended content types for specific uses.
